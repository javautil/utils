package de.microtema.commons.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Enum Util ...
 */
public final class EnumUtil {

    private static final String SEPARATOR = ",";

    private EnumUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    /**
     * Join enums names to String by comma ','
     *
     * @param enums to be joined
     * @return String
     */
    public static String join(Enum[] enums) {
        if (enums == null) {
            return null;
        }

        List<String> names = Arrays.stream(enums).map(Enum::toString).collect(Collectors.toList());

        return StringUtils.join(names, SEPARATOR);
    }

    /**
     * Join enums names to String by comma ',' by given enum
     *
     * @param enumType to join
     * @return String
     */
    public static String join(Enum enumType) {
        if (enumType == null) {
            return null;
        }

        return join(enumType.getClass().getEnumConstants());
    }

    /**
     * Get Enum or default Enum if not found
     *
     * @param values      may not be null
     * @param enumName    to search for
     * @param defaultEnum as default return value
     * @param <E>         required enum type
     * @return Enum or default Enum if not found
     */
    public static <E extends Enum> E getOne(E[] values, String enumName, E defaultEnum) {
        Validate.notNull(values);

        for (E element : values) {
            if (StringUtils.equals(element.name(), enumName)) {
                return element;
            }
        }

        return defaultEnum;
    }

    /**
     * Get Enum or throw NoSuchElementException if not found
     *
     * @param values   may not be null
     * @param enumName to search for
     * @param <E>      required enum type
     * @return Enum or throw NoSuchElementException
     */
    public static <E extends Enum> E getOne(E[] values, String enumName) {

        return Optional.ofNullable(findOne(values, enumName)).orElseThrow(() -> new NoSuchElementException(
                "Unable to get Element by " + "name: " + enumName + " on values: " + values));
    }

    /**
     * Get Enum by ignoring case or throw NoSuchElementException if not found
     *
     * @param values   may not be null
     * @param enumName to search for
     * @param <E>      required enum type
     * @return Enum or throw NoSuchElementException
     */
    public static <E extends Enum> E getOneIgnoreCase(E[] values, String enumName) {
        Validate.notNull(values);

        return Optional.ofNullable(findOneIgnoreCase(values, enumName)).orElseThrow(() -> new NoSuchElementException(
                "Unable to get Element by " + "name: " + enumName + " on values: " + values));
    }

    /**
     * Get Enum by ignoring case or default if not found
     *
     * @param values      may not be null
     * @param enumName    to search for
     * @param <E>         required enum type
     * @param defaultEnum default Enum value
     * @return Enum
     */
    public static <E extends Enum> E getOneIgnoreCase(E[] values, String enumName, E defaultEnum) {
        Validate.notNull(values);

        for (E element : values) {
            if (StringUtils.equalsIgnoreCase(element.name(), enumName)) {
                return element;
            }
        }

        return defaultEnum;
    }

    /**
     * Find Enum or return null if not found
     *
     * @param values   may not be null
     * @param enumName to search for
     * @param <E>      required enum type
     * @return Enum
     */
    public static <E extends Enum> E findOne(E[] values, String enumName) {
        Validate.notNull(values);

        for (E element : values) {
            if (StringUtils.equals(element.name(), enumName)) {
                return element;
            }
        }

        return null;
    }

    /**
     * Find Enum  ignoring case or return null if not found
     *
     * @param values   may not be null
     * @param enumName to search for
     * @param <E>      required enum type
     * @return Enum
     */
    public static <E extends Enum> E findOneIgnoreCase(E[] values, String enumName) {
        Validate.notNull(values);

        for (E element : values) {
            if (StringUtils.equalsIgnoreCase(element.name(), enumName)) {
                return element;
            }
        }

        return null;
    }

    /**
     * @param element may not be null
     * @param values  may not be null
     * @param <E>     enum type
     * @return true if element is in values
     */
    public static <E extends Enum> boolean is(E element, E... values) {
        Validate.notNull(element);

        if (values == null) {
            return false;
        }

        for (E entry : values) {
            if (entry == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param element may not be null
     * @param values  may not be null
     * @param <E>     enum type
     * @return true if element ordinal is les that any in values
     */
    public static <E extends Enum> boolean isLessThen(E element, E... values) {
        Validate.notNull(element);
        Validate.notNull(values);

        List<E> list = Stream.of(values).sorted().distinct().filter(it -> it != element).collect(Collectors.toList());

        E first = CollectionUtil.first(list);

        return first != null && element.ordinal() < first.ordinal();
    }

    /**
     * @param element may not be null
     * @param values  may not be null
     * @param <E>     enum type
     * @return true if element ordinal is greater that any in values
     */
    public static <E extends Enum> boolean isGreaterThen(E element, E... values) {
        Validate.notNull(element);
        Validate.notNull(values);

        List<E> list = Stream.of(values).sorted().distinct().filter(it -> it != element).collect(Collectors.toList());

        E last = CollectionUtil.last(list);

        return last != null && element.ordinal() > last.ordinal();
    }

    /**
     * @param values may not be null
     * @param <E>    enum type
     * @return true if element ordinal is greater that any in values
     */
    public static <E extends Enum> E getMax(E... values) {
        Validate.notNull(values);

        List<E> list = Stream.of(values).filter(Objects::nonNull).sorted().distinct().collect(Collectors.toList());

        return CollectionUtil.last(list);
    }

    /**
     * @param type may not be null
     * @param <E>  enum type
     * @return true if element ordinal is greater that any in values
     */
    public static <E extends Enum> E getMax(Class<E> type) {
        Validate.notNull(type);

        List<E> list = Stream.of(type.getEnumConstants()).sorted().distinct().collect(Collectors.toList());

        return CollectionUtil.last(list);
    }

    /**
     * @param values may not be null
     * @param <E>    enum type
     * @return true if element ordinal is greater that any in values
     */
    public static <E extends Enum> E getMin(E... values) {
        Validate.notNull(values);

        List<E> list = Stream.of(values).filter(Objects::nonNull).sorted().distinct().collect(Collectors.toList());

        return CollectionUtil.first(list);
    }

    /**
     * @param type may not be null
     * @param <E>  enum type
     * @return true if element ordinal is greater that any in values
     */
    public static <E extends Enum> E getMin(Class<E> type) {
        Validate.notNull(type);

        List<E> list = Stream.of(type.getEnumConstants()).sorted().distinct().collect(Collectors.toList());

        return CollectionUtil.first(list);
    }

    /**
     * @param values may not be null
     * @param <E>    enum type
     * @return first position element from values
     */
    public static <E extends Enum> E firstOne(E[] values) {
        Validate.notNull(values);
        Validate.notEmpty(values);

        return values[0];
    }

    /**
     * @param values may not be null
     * @param <E>    enum type
     * @return first position element from values
     */
    public static <E extends Enum> E lastOne(E[] values) {
        Validate.notNull(values);
        Validate.notEmpty(values);

        return values[values.length - 1];
    }

    /**
     * @param element      may not be null
     * @param propertyName may not be empty
     * @param <E>          enum type
     * @param <V>          value type
     * @return field value or throw Exception
     */
    public static <E extends Enum, V> V getProperty(E element, String propertyName) {
        Validate.notNull(element);
        Validate.notEmpty(propertyName);

        Field field = FieldUtils.getDeclaredField(element.getClass(), propertyName, true);

        return FieldUtil.getFieldValue(field, element);
    }

    /**
     * @param element      may not be null
     * @param propertyName may not be empty
     * @param defaultValue may be null
     * @param <E>          enum type
     * @param <V>          value type
     * @return field value or throw Exception
     */
    public static <E extends Enum, V> V getPropertyOrDefault(E element, String propertyName, V defaultValue) {
        Validate.notNull(element);
        Validate.notEmpty(propertyName);

        Field field = FieldUtils.getDeclaredField(element.getClass(), propertyName, true);

        if (field == null) {
            return defaultValue;
        }

        return FieldUtil.getFieldValue(field, element);
    }

    /**
     * @param element may not be null
     * @param <E>     enum type
     * @return field value or throw Exception
     */
    public static <E extends Enum> String getDisplayName(E element) {
        Validate.notNull(element);

        return getProperty(element, "displayName");
    }
}
