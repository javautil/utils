package de.microtema.commons.utils;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.Validate;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;


/**
 * Common Bean Util
 */
public final class BeanUtil {

    private BeanUtil() {
        throw new UnsupportedOperationException(getClass().getName() + " should not be called with new!");
    }


    /**
     * @param bean         should not be null
     * @param propertyName should not be null
     * @param <T>          required type
     * @return property value or throw IllegalArgumentException
     */
    @SuppressWarnings("unchecked")
    public static <T> T getSimpleProperty(Object bean, String propertyName) {
        Validate.notNull(bean);
        Validate.notNull(propertyName);

        try {
            return (T) PropertyUtils.getSimpleProperty(bean, propertyName);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new IllegalArgumentException("Unable to resolve Expression", e);
        }
    }

    /**
     * @param bean          should not be null
     * @param propertyName  should not be null
     * @param propertyValue can be null
     */
    public static void setProperty(Object bean, String propertyName, Object propertyValue) {
        Validate.notNull(bean);
        Validate.notNull(propertyName);

        try {
            PropertyUtils.setProperty(bean, propertyName, propertyValue);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new IllegalArgumentException("Unable to set value", e);
        }
    }

    /**
     * @param bean to be copy
     * @param <T>  required type
     * @return new Instance that is exactly the same as given
     */
    @SuppressWarnings("unchecked")
    public static <T> T copy(T bean) {
        if (bean == null) {
            return null;
        }

        if (bean instanceof Serializable) {
            return (T) SerializationUtils.clone((Serializable) bean);
        }

        T instance = ClassUtil.createInstance((Class<T>) bean.getClass());

        try {
            PropertyUtils.copyProperties(instance, bean);
        } catch (Exception e) {
            throw new IllegalArgumentException("Unable to copy properties from bean: " + bean, e);
        }

        return instance;
    }
}
