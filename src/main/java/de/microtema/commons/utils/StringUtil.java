package de.microtema.commons.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

public final class StringUtil {

    private StringUtil() {
        throw new UnsupportedOperationException(getClass().getName() + " should not be called with new!");
    }

    public static String notBlank(String... strings) {

        return Stream.of(strings).filter(StringUtils::isNotBlank).findAny().orElse(null);
    }

    public static String notEmpty(String... strings) {

        return Stream.of(strings).filter(StringUtils::isNotEmpty).findAny().orElse(null);
    }
}
