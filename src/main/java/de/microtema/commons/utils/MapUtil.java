package de.microtema.commons.utils;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.Validate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Map Util
 */
public final class MapUtil {

    private MapUtil() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    /**
     * @param map   may not be null
     * @param key   may not be null
     * @param value to put
     * @param <K>   required key type
     * @param <V>   required value type
     */
    public static <K, V> void putIgnoreNull(Map<K, V> map, K key, V value) {
        Validate.notNull(map);
        Validate.notNull(key);

        if (Objects.isNull(value)) {
            return;
        }

        map.put(key, value);
    }

    /**
     * Create new Map for given arguments
     *
     * @param arguments to map
     * @param <K>       required key type
     * @param <V>       required value type
     * @return Map
     */
    @SuppressWarnings("unchecked")
    public static <K, V> Map<K, V> asMap(Object... arguments) {
        if (arguments == null || arguments.length == 0) {
            return Collections.emptyMap();
        }

        Map<K, V> map = new HashMap<>();

        for (int index = 0; index < arguments.length; index = index + 2) {

            K key = (K) arguments[index];

            V value = (V) arguments[index + 1];

            putIgnoreNull(map, key, value);
        }

        return map;
    }

    /**
     * Get Property value related by name
     *
     * @param map          may not be null
     * @param propertyName may not be null
     * @param type         may not be null
     * @param <T>          required type
     * @return Property value
     */
    @SuppressWarnings("unchecked")
    public static <T> T getProperty(Map<String, ?> map, String propertyName, Class<T> type) {
        Validate.notNull(map);
        Validate.notNull(propertyName);

        Object value = map.get(propertyName);

        if (value == null) {
            // lets avoid NullPointerException when converting to boolean for null values
            if (boolean.class.isAssignableFrom(type) || Boolean.class.isAssignableFrom(type)) {
                return (T) Boolean.FALSE;
            }

            if (int.class.isAssignableFrom(type)) {
                return (T) Integer.valueOf(0);
            }

            if (long.class.isAssignableFrom(type)) {
                return (T) Long.valueOf(0);
            }

            if (double.class.isAssignableFrom(type)) {
                return (T) Double.valueOf(0);
            }

            return null;
        }

        return castValue(value, type);
    }

    /**
     * Get Property or return default value if not present
     *
     * @param map          may not be null
     * @param propertyName may not be null
     * @param type         may not be null
     * @param defaultValue if none found
     * @param <T>          required type
     * @return Object
     */
    @SuppressWarnings("unchecked")
    public static <T> T getProperty(Map<String, ?> map, String propertyName, Class<T> type, T defaultValue) {
        Validate.notNull(map);
        Validate.notNull(propertyName);

        Object value = map.get(propertyName);

        if (value == null) {
            return defaultValue;
        }

        return castValue(value, type);
    }

    /**
     * trim To Empty if null
     *
     * @param map to trim
     * @param <K> required key type
     * @param <V> required key type
     * @return Map
     */
    public static <K, V> Map<K, V> trimToEmpty(Map<K, V> map) {

        if (map == null) {
            return Collections.emptyMap();
        }

        return map;
    }

    /**
     * @param target may not be null
     * @param source may be null or empty
     * @param <K>    key type
     * @param <V>    value type
     */
    public static <K, V> void overrideValues(Map<K, V> target, Map<K, V> source) {
        Validate.notNull(target);

        if (MapUtils.isEmpty(source)) {
            return;
        }

        target.entrySet().stream().filter(it -> source.containsKey(it.getKey())).forEach(it -> it.setValue(source.get(it.getKey())));
    }

    /**
     * merge source to target using merging the object Processor
     *
     * @param target may not be null
     * @param source may be null or empty
     * @param <K>    key type
     * @param <V>    value type
     */
    public static <K, V> void mergeValues(Map<K, V> target, Map<K, V> source, ObjectProcessor<K, V> objectProcessor) {
        Validate.notNull(target);
        Validate.notNull(objectProcessor);

        if (MapUtils.isEmpty(source)) {
            return;
        }

        source.forEach((key, value) -> {

            V answer = objectProcessor.process(key, target.get(key), value);

            if (answer == null) {
                target.remove(key);
            } else {
                target.put(key, answer);
            }
        });
    }

    public interface ObjectProcessor<K, V> {

        V process(K key, V oldValue, V newValue);
    }

    @SuppressWarnings("unchecked")
    private static <T> T castValue(Object value, Class<T> type) {
        assert value != null;
        assert type != null;

        if (!(value instanceof String)) {
            return (T) value;
        }

        if (boolean.class.isAssignableFrom(type) || Boolean.class.isAssignableFrom(type)) {
            return (T) Boolean.valueOf(value.toString());
        }

        if (long.class.isAssignableFrom(type) || Long.class.isAssignableFrom(type)) {
            return (T) Long.valueOf(value.toString());
        }

        if (int.class.isAssignableFrom(type) || Integer.class.isAssignableFrom(type)) {
            return (T) Integer.valueOf(value.toString());
        }

        if (type.isEnum()) {
            return (T) EnumUtil.getOne((Enum[]) type.getEnumConstants(), value.toString());
        }

        return (T) value;
    }
}
