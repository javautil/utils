package de.microtema.commons.utils;

import de.microtema.commons.utils.model.Person;
import org.junit.Test;

import javax.xml.bind.annotation.XmlAttribute;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FieldUtilTest {

    FieldUtil sut;

    @XmlAttribute(required = true)
    String test = UUID.randomUUID().toString();

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<FieldUtil> constructor = FieldUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void getPropertyFields() {

        List<Field> propertyFields = FieldUtil.getPropertyFields(Person.class);

        List<String> fieldNames = propertyFields.stream().map(Field::getName).collect(Collectors.toList());

        assertEquals(2, fieldNames.size());
        assertTrue(fieldNames.contains("firstName"));
        assertTrue(fieldNames.contains("emails"));
    }

    @Test
    public void isRequiredField() throws NoSuchFieldException {

        Field field = Person.class.getDeclaredField("firstName");

        assertFalse(FieldUtil.isRequiredField(field));
    }

    @Test
    public void isRequiredFieldReturnTrue() throws NoSuchFieldException {

        Field field = FieldUtilTest.class.getDeclaredField("test");

        assertTrue(FieldUtil.isRequiredField(field));
    }

    @Test
    public void getFieldValue() throws NoSuchFieldException {

        Field field = FieldUtilTest.class.getDeclaredField("test");

        assertEquals(test, FieldUtil.getFieldValue(field, this));
    }

    @Test
    public void doWithFields() throws Exception {

        Field testField = FieldUtilTest.class.getDeclaredField("test");

        final List<Field> matchFields = new ArrayList<>();

        FieldUtil.doWithFields(FieldUtilTest.class, field -> {
                    matchFields.add(field);
                }, field ->
                        field.isAnnotationPresent(XmlAttribute.class)
        );

        assertFalse(matchFields.isEmpty());
        assertEquals(1, matchFields.size());
        assertEquals(testField, matchFields.get(0));
    }

    @Test
    public void makeAccessible() throws Exception {

        Field field = FieldUtilTest.class.getDeclaredField("test");

        assertFalse(field.isAccessible());

        FieldUtil.makeAccessible(field);

        assertTrue(field.isAccessible());
    }

    @Test
    public void setFieldValue() throws Exception {

        Field field = FieldUtilTest.class.getDeclaredField("test");

        FieldUtil.setFieldValue(field, this, "Foo");

        assertEquals("Foo", FieldUtil.getFieldValue(field, this));
    }
}
