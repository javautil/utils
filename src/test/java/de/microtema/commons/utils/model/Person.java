package de.microtema.commons.utils.model;

import java.util.Objects;

public class Person {

    private boolean created;

    private String firstName;

    private String[] emails;

    public String getFirstName() {
        return firstName;
    }

    public String[] getEmails() {
        return emails;
    }

    public void setEmails(String[] emails) {
        this.emails = emails;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {

        return "Name: " + this.firstName;
    }

    public void setName(String firstName, String lastName) {
        this.firstName = firstName + " : " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName);
    }
}
