package de.microtema.commons.utils;

import de.microtema.commons.utils.enums.ArchiveMode;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class EnumUtilTest {

    EnumUtil sut;

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<EnumUtil> constructor = EnumUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void getOneOrDefault() {
        assertSame(ArchiveMode.NONE, EnumUtil.getOne(ArchiveMode.values(), "NONE", ArchiveMode.NONE));
    }

    @Test
    public void getOneIgnoreCaseOrDefault() {
        assertSame(ArchiveMode.NONE, EnumUtil.getOneIgnoreCase(ArchiveMode.values(), "none", ArchiveMode.NONE));
    }

    @Test
    public void getOne() {
        assertSame(ArchiveMode.NONE, EnumUtil.getOne(ArchiveMode.values(), "NONE"));
    }

    @Test(expected = NoSuchElementException.class)
    public void getOneWillThrowNoSuchElementException() {
        EnumUtil.getOne(ArchiveMode.values(), "Foo");
    }

    @Test
    public void getOneIgnoreCase() {
        assertSame(ArchiveMode.NONE, EnumUtil.getOneIgnoreCase(ArchiveMode.values(), "NONE"));
    }

    @Test
    public void findOneIgnoreCase() {
        assertSame(ArchiveMode.NONE, EnumUtil.getOneIgnoreCase(ArchiveMode.values(), "None"));
    }

    @Test(expected = NoSuchElementException.class)
    public void getOneIgnoreCaseWillThrowNoSuchElementException() {
        EnumUtil.getOneIgnoreCase(ArchiveMode.values(), "Foo");
    }

    @Test
    public void join() {
        assertEquals("FILE,DOCUMENT_STORE,NONE", EnumUtil.join(ArchiveMode.values()));
    }


    @Test
    public void joinOnNullEnums() {
        assertNull(EnumUtil.join((Enum[]) null));
    }

    @Test
    public void joinOnNullEnum() {
        assertNull(EnumUtil.join((Enum) null));
    }

    @Test
    public void isInValues() {

        assertTrue(EnumUtil.is(ArchiveMode.FILE, ArchiveMode.values()));
    }

    @Test
    public void isSame() {

        assertTrue(EnumUtil.is(ArchiveMode.FILE, ArchiveMode.FILE));
    }

    @Test
    public void isNotSame() {

        assertFalse(EnumUtil.is(ArchiveMode.FILE, ArchiveMode.DOCUMENT_STORE));
    }

    @Test
    public void isNotOnEmptyElements() {

        assertFalse(EnumUtil.is(ArchiveMode.FILE));
    }

    @Test
    public void isNotOnNull() {

        assertFalse(EnumUtil.is(ArchiveMode.FILE, null));
    }

    @Test(expected = NullPointerException.class)
    public void beforeWillReturnNullPointerException() {

        assertTrue(EnumUtil.isLessThen(ArchiveMode.FILE, null));
    }

    @Test
    public void lessThen() {

        assertTrue(EnumUtil.isLessThen(ArchiveMode.FILE, ArchiveMode.DOCUMENT_STORE));
    }

    @Test
    public void lessThenOnSame() {

        assertFalse(EnumUtil.isLessThen(ArchiveMode.DOCUMENT_STORE, ArchiveMode.DOCUMENT_STORE));
    }

    @Test
    public void lessThenReturnFalse() {

        assertFalse(EnumUtil.isLessThen(ArchiveMode.DOCUMENT_STORE, ArchiveMode.FILE));
    }

    @Test(expected = NullPointerException.class)
    public void greaterThenWillReturnNullPointerException() {

        assertFalse(EnumUtil.isGreaterThen(ArchiveMode.FILE, null));
    }

    @Test
    public void greaterThen() {

        assertFalse(EnumUtil.isGreaterThen(ArchiveMode.FILE, ArchiveMode.DOCUMENT_STORE));
        assertTrue(EnumUtil.isGreaterThen(ArchiveMode.DOCUMENT_STORE, ArchiveMode.FILE));
    }

    @Test
    public void greaterThenOnSame() {

        assertFalse(EnumUtil.isGreaterThen(ArchiveMode.DOCUMENT_STORE, ArchiveMode.DOCUMENT_STORE));
    }

    @Test
    public void greaterThenReturnFalse() {

        assertFalse(EnumUtil.isGreaterThen(ArchiveMode.FILE, ArchiveMode.DOCUMENT_STORE));
    }

    @Test
    public void firstOne() {

        assertSame(ArchiveMode.FILE, EnumUtil.firstOne(ArchiveMode.values()));
    }

    @Test
    public void lastOne() {

        assertSame(ArchiveMode.NONE, EnumUtil.lastOne(ArchiveMode.values()));
    }

    @Test
    public void getProperty() {

        assertSame("File", EnumUtil.getProperty(ArchiveMode.FILE, "displayName"));
    }

    @Test
    public void getPropertyOrDefault() {

        assertEquals("File", EnumUtil.getPropertyOrDefault(ArchiveMode.FILE, "XX", "File"));
    }

    @Test
    public void getDisplayName() {

        assertSame("Document Store", EnumUtil.getDisplayName(ArchiveMode.DOCUMENT_STORE));
    }

    @Test
    public void getMaxOnEmpty() {

        assertNull(EnumUtil.getMax());
    }

    @Test
    public void getMaxOnNull() {

        assertNull(EnumUtil.getMax((ArchiveMode) null));
    }

    @Test
    public void getMax() {

        assertSame(ArchiveMode.NONE, EnumUtil.getMax(ArchiveMode.DOCUMENT_STORE, ArchiveMode.FILE, ArchiveMode.NONE));
    }

    @Test
    public void getMaxOnType() {

        assertSame(ArchiveMode.NONE, EnumUtil.getMax(ArchiveMode.class));
    }


    @Test
    public void getMinOnNull() {

        assertNull(EnumUtil.getMin((ArchiveMode) null));
    }

    @Test
    public void getMin() {

        assertSame(ArchiveMode.FILE, EnumUtil.getMin(ArchiveMode.DOCUMENT_STORE, ArchiveMode.FILE, ArchiveMode.NONE));
    }

    @Test
    public void getMinOnType() {

        assertSame(ArchiveMode.FILE, EnumUtil.getMin(ArchiveMode.class));
    }

    @Test
    public void getMinOnEmpty() {

        assertNull(EnumUtil.getMax());
    }
}
