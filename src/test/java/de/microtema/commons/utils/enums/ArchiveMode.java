package de.microtema.commons.utils.enums;

public enum ArchiveMode {
    FILE("File"), DOCUMENT_STORE("Document Store"), NONE("None");

    public String getDisplayName() {
        return displayName;
    }

    String displayName;

    ArchiveMode(String displayName) {
        this.displayName = displayName;
    }
}
