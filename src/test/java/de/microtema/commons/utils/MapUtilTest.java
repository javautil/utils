package de.microtema.commons.utils;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

public class MapUtilTest {

    MapUtil sut;

    Map<String, Object> model = new HashMap<>();

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<MapUtil> constructor = MapUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void putIgnoreNull() {

        MapUtil.putIgnoreNull(model, "ignore.null", null);

        assertTrue(model.isEmpty());
    }

    @Test
    public void asMap() {

        Map<String, String> map = MapUtil.asMap("one", "one", "two", null);

        assertFalse(map.isEmpty());
        assertEquals(1, map.size());
        assertEquals("one", map.get("one"));
    }

    @Test
    public void getPropertyAsString() {

        Map<String, String> map = MapUtil.asMap("one", "one", "two", null);

        assertEquals("one", MapUtil.getProperty(map, "one", String.class));
    }

    @Test
    public void getPropertyAsBoolean() {

        Map<String, String> map = Collections.emptyMap();

        assertFalse(MapUtil.getProperty(map, "one", Boolean.class));
    }

    @Test
    public void getPropertyAsInteger() {

        Map<String, String> map = Collections.emptyMap();
        int count = MapUtil.getProperty(map, "one", int.class);
        assertEquals(0, count);
    }

    @Test
    public void getPropertyAsLong() {

        Map<String, String> map = Collections.emptyMap();
        long count = MapUtil.getProperty(map, "one", long.class);
        assertEquals(0, count);
    }

    @Test
    public void getPropertyAsBooleanPrimitive() {

        Map<String, String> map = MapUtil.asMap("one", true, "two", null);

        assertTrue(MapUtil.getProperty(map, "one", Boolean.class));
    }

    @Test
    public void getPropertyAsBooleanFromString() {

        Map<String, String> map = MapUtil.asMap("one", "true");

        assertTrue(MapUtil.getProperty(map, "one", Boolean.class));
    }

    @Test
    public void getPropertyAsBooleanPrimitiveFromString() {

        Map<String, String> map = MapUtil.asMap("one", "true");

        assertTrue(MapUtil.getProperty(map, "one", boolean.class));
    }

    @Test
    public void getPropertyAsBooleanWithDefaultValue() {

        Map<String, String> map = Collections.emptyMap();

        assertFalse(MapUtil.getProperty(map, "one", Boolean.class, Boolean.FALSE));
    }

    @Test
    public void getPropertyAsBooleanWithDefaultValueAsNull() {

        Map<String, String> map = Collections.emptyMap();

        assertNull(MapUtil.getProperty(map, "one", Boolean.class, null));
    }


    @Test
    public void trimToEmptyOnNull() {

        Map<String, String> map = null;

        assertTrue(MapUtil.trimToEmpty(map).isEmpty());
    }

    @Test
    public void trimToEmptyOnEmpty() {

        Map<String, String> map = Collections.emptyMap();

        assertSame(MapUtil.trimToEmpty(map), map);
    }


    @Test
    public void getPropertyAsInt() {

        Map<String, String> map = MapUtil.asMap("one", 1, "two", null);

        assertEquals(new Integer(1), MapUtil.getProperty(map, "one", Integer.class));
    }

    @Test
    public void getPropertyAsIntString() {

        Map<String, String> map = MapUtil.asMap("one", "1", "two", null);

        assertEquals(new Integer(1), MapUtil.getProperty(map, "one", Integer.class));
    }

    @Test
    public void getPropertyAsEnum() {

        Map<String, String> map = MapUtil.asMap("one", "BAR");

        assertEquals(Foo.BAR, MapUtil.getProperty(map, "one", Foo.class));
    }

    @Test
    public void overrideValuesOnEmptyMap() {

        Map<String, String> target = MapUtil.asMap("one", "BAR");
        Map<String, String> source = MapUtil.asMap();

        MapUtil.overrideValues(target, source);

        assertEquals(MapUtil.asMap("one", "BAR"), target);
    }

    @Test
    public void overrideValuesOnNullMap() {

        Map<String, String> target = MapUtil.asMap("one", "BAR");

        MapUtil.overrideValues(target, null);

        assertEquals(MapUtil.asMap("one", "BAR"), target);
    }

    @Test
    public void overrideValuesOnDifferentEntry() {

        Map<String, String> target = MapUtil.asMap("one", "BAR");
        Map<String, String> source = MapUtil.asMap("second", "FOO");

        MapUtil.overrideValues(target, source);

        assertEquals(MapUtil.asMap("one", "BAR"), target);
    }

    @Test
    public void mergeValues() {

        Map<String, Set<String>> target = MapUtil.asMap("one", Collections.singleton("BAR"));
        Map<String, Set<String>> source = MapUtil.asMap("one", Collections.singleton("FOO"));

        MapUtil.mergeValues(target, source, (key, oldValue, newValue) -> Stream.concat(oldValue.stream(), newValue.stream()).collect(Collectors.toSet()));

        assertEquals(new HashSet<>(Arrays.asList("BAR", "FOO")), target.get("one"));
    }

    @Test
    public void mergeValuesOnMissingKeys() {

        Map<String, Set<String>> target = MapUtil.asMap("one", Collections.singleton("BAR"));
        Map<String, Set<String>> source = MapUtil.asMap("two", Collections.singleton("FOO"));

        MapUtil.mergeValues(target, source, (key, oldValue, newValue) -> Stream.concat(CollectionUtil.trimToEmpty(oldValue).stream(), CollectionUtil.trimToEmpty(newValue).stream()).collect(Collectors.toSet()));

        assertEquals(new HashSet<>(Arrays.asList("BAR")), target.get("one"));
    }

    enum Foo {
        BAR
    }
}
