package de.microtema.commons.utils;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.assertEquals;

public class StringUtilTest {

    StringUtil sut;

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<StringUtil> constructor = StringUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void notBlank() {

        assertEquals("foo", StringUtil.notBlank("", " ", null, "foo", "bar"));
    }

    @Test
    public void notBlankWillNotTrim() {

        assertEquals(" foo ", StringUtil.notBlank("", " ", null, " foo ", "bar"));
    }


    @Test
    public void notEmptyWillReturnEmptyString() {

        assertEquals(" ", StringUtil.notEmpty("", " ", null, "foo", "bar"));
    }

    @Test
    public void notEmpty() {

        assertEquals(" foo ", StringUtil.notEmpty("", " foo ", " ", null, "foo", "bar"));
    }

    @Test
    public void notEmptyWillNotTrim() {

        assertEquals(" ", StringUtil.notEmpty("", " ", null, " foo ", "bar"));
    }
}
