package de.microtema.commons.utils;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;

import static junit.framework.TestCase.assertTrue;

public class ExceptionUtilTest {

    ExceptionUtil sut;

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<ClassUtil> constructor = ClassUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test(expected = UndeclaredThrowableException.class)
    public void handleReflectionException() {

        Exception exception = new Exception();

        ExceptionUtil.handleReflectionException(exception);
    }

    @Test(expected = IllegalStateException.class)
    public void handleNoSuchMethodExceptionException() {

        Exception exception = new NoSuchMethodException();

        ExceptionUtil.handleReflectionException(exception);
    }

    @Test(expected = IllegalStateException.class)
    public void handleIllegalAccessException() {

        Exception exception = new IllegalAccessException();

        ExceptionUtil.handleReflectionException(exception);
    }

    @Test(expected = RuntimeException.class)
    public void handleRuntimeException() {

        Exception exception = new IllegalAccessException();

        ExceptionUtil.handleReflectionException(exception);
    }

    @Test(expected = RuntimeException.class)
    public void handleInvocationTargetException() {

        InvocationTargetException exception = new InvocationTargetException(new RuntimeException());

        ExceptionUtil.handleInvocationTargetException(exception);
    }

    @Test(expected = RuntimeException.class)
    public void rethrowRuntimeException() {
        RuntimeException exception = new RuntimeException();

        ExceptionUtil.rethrowRuntimeException(exception);
    }

    @Test(expected = Error.class)
    public void rethrowRuntimeExceptionOnError() {

        Error exception = new Error();

        ExceptionUtil.rethrowRuntimeException(exception);
    }

    @Test(expected = RuntimeException.class)
    public void rethrowRuntimeExceptionOnUndeclaredThrowableException() {

        UndeclaredThrowableException exception = new UndeclaredThrowableException(new RuntimeException());

        ExceptionUtil.rethrowRuntimeException(exception);
    }

    @Test(expected = Exception.class)
    public void rethrowException() throws Exception {

        Exception exception = new Exception();

        ExceptionUtil.rethrowException(exception);
    }

    @Test(expected = Error.class)
    public void rethrowExceptionOnError() throws Exception {

        Error exception = new Error();

        ExceptionUtil.rethrowException(exception);
    }

    @Test(expected = RuntimeException.class)
    public void rethrowExceptionOnRuntimeException() throws Exception {

        RuntimeException exception = new RuntimeException();

        ExceptionUtil.rethrowException(exception);
    }

    @Test
    public void declaresException() throws Exception {

        Method method = getClass().getMethod("declaresException");

        assertTrue(ExceptionUtil.declaresException(method, Exception.class));
    }

    @Test
    public void declaresExceptionWillReturnFalse() throws Exception {

        Method method = getClass().getMethod("declaresException");

        assertTrue(ExceptionUtil.declaresException(method, IllegalArgumentException.class));
    }


}
