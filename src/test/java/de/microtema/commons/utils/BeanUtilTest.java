package de.microtema.commons.utils;

import de.microtema.commons.utils.model.Person;
import org.junit.Test;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

public class BeanUtilTest {

    BeanUtil sut;

    Person model = new Person();

    @Test(expected = UnsupportedOperationException.class)
    public void utilityClassTest() throws Exception {

        Constructor<BeanUtil> constructor = BeanUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        try {
            constructor.newInstance();
        } catch (InvocationTargetException e) {
            throw (UnsupportedOperationException) e.getTargetException();
        }
    }

    @Test
    public void getSimpleProperty() {

        model.setFirstName("firstName");

        assertEquals(model.getFirstName(), BeanUtil.getSimpleProperty(model, "firstName"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void getSimplePropertyWillThrowIllegalArgumentException() {

        BeanUtil.getSimpleProperty(model, "unknow");
    }

    @Test(expected = IllegalArgumentException.class)
    public void setPropertyWillThrowIllegalArgumentException() {

        BeanUtil.setProperty(model, "unknow", null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setPropertyWillThrowIllegalArgumentExceptionONWrongType() {

        Object firstName = Boolean.FALSE;

        BeanUtil.setProperty(model, "firstName", firstName);
    }

    @Test
    public void setProperty() {

        String firstName = "Foo";

        BeanUtil.setProperty(model, "firstName", firstName);

        assertEquals(firstName, model.getFirstName());
    }

    @Test
    public void copyOnNull() {

        assertNull(BeanUtil.copy(null));
    }

    @Test
    public void copy() {

        Person copy = BeanUtil.copy(model);

        assertNotNull(copy);
        assertNotSame(model, copy);
        assertEquals(model, copy);
    }

    @Test
    public void copyWillThrowException() {

        BeanUtil.copy(new Group());
    }

    public static class Group implements Serializable{

    }

}
